package br.com.qarest.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.qarest.models.Pessoa;
@Path("/pessoas")
public class PessoaResource {
	private static Map<Integer, Pessoa> pessoas;
	
	static {
		pessoas = new HashMap<Integer, Pessoa>();
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("Weslei Rodrigues Dias");
		pessoa.setDataNascimento("05/10/1990");
		pessoa.setSexo("Masculino");
		pessoa.setEndereco("Rua s�o Luiz n� 761, apt 101");
		
		pessoas.put(pessoa.getId(), pessoa);
	}

	@GET
	@Produces("text/xml")
	public List<Pessoa> getPessoas() {
		return new ArrayList<Pessoa>(pessoas.values());
	}

	@Produces("text/xml")
	public Pessoa getPessoa(@PathParam("id") int id) {
		return pessoas.get(id);
	}

	@POST
	@Consumes("text/xml")
	@Produces("text/plain")
	public String adicionarPessoa(Pessoa pessoa) {
		pessoa.setId(pessoas.size() + 1);
		pessoas.put(pessoa.getId(), pessoa);
		return pessoa.getNome() + " adicionado.";
	}

	@Path("{id}")
	@PUT
	@Consumes("text/xml")
	@Produces("text/plain")
	public String atualizarPessoa(Pessoa pessoa, @PathParam("id") int id) {
		Pessoa pessoaAtual = pessoas.get(id);
		pessoaAtual.setNome(pessoa.getNome());
		pessoaAtual.setDataNascimento(pessoa.getDataNascimento());
		pessoaAtual.setEndereco(pessoa.getEndereco());
		pessoaAtual.setSexo(pessoa.getSexo());
		return pessoa.getNome() + " atualizada.";
	}

	@Path("{id}")
	@DELETE
	@Produces("text/plain")
	public String removePessoa(@PathParam("id") int id) {
		pessoas.remove(id);
		return "Pessoa removida.";
	}
	

}
